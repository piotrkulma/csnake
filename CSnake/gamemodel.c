#include "gamemodel.h"

GameModelPtr initGameModel()
{
	GameModelPtr gameModel = malloc(sizeof(GameModel) * 1);
	gameModel->game = TRUE;
	gameModel->pause = FALSE;

	gameModel->score = 0;

	gameModel->actualDirection = RIGHT;
	gameModel->snake = initSnake();

	gameModel->food = malloc(sizeof(COORD) * MAX_FOOD_ELEMENTS_ON_SCREEN);
	gameModel->food[0].X = 10;
	gameModel->food[0].Y = 10;
	gameModel->food[1].X = 5;
	gameModel->food[1].Y = 5;
	gameModel->food[2].X = 8;
	gameModel->food[2].Y = 8;
	gameModel->food[3].X = 9;
	gameModel->food[3].Y = 3;
	gameModel->food[4].X = 9;
	gameModel->food[4].Y = 4;
	gameModel->food[5].X = 9;
	gameModel->food[5].Y = 5;
	gameModel->food[6].X = 9;
	gameModel->food[6].Y = 6;

	return gameModel;
}

void updateGameState(GameModelPtr gameModel, int key)
{
	if (key == KEY_ARROW_UP && gameModel->actualDirection != DOWN)
	{
		gameModel->actualDirection = UP;
	}
	else if (key == KEY_ARROW_RIGHT && gameModel->actualDirection != LEFT)
	{
		gameModel->actualDirection = RIGHT;
	}
	else if (key == KEY_ARROW_DOWN && gameModel->actualDirection != UP)
	{
		gameModel->actualDirection = DOWN;
	}
	else if (key == KEY_ARROW_LEFT && gameModel->actualDirection != RIGHT)
	{
		gameModel->actualDirection = LEFT;
	}
	else if (key == KEY_ESC)
	{
		gameModel->game = FALSE;
	}
	else if (key == KEY_ENTER)
	{
		if (gameModel->pause)
		{
			gameModel->pause = FALSE;
		}
		else
		{
			gameModel->pause = TRUE;
		}
	}
}

void drawSnakeFood(GameModelPtr gameModel)
{
	for (int i = 0; i < MAX_FOOD_ELEMENTS_ON_SCREEN; i++)
	{
		gotoxy(gameModel->food[i].X, gameModel->food[i].Y);
		putch(FOOD_ELEMENT);
	}
}

void drawSnake(GameModelPtr gameModel)
{
	SnakeElementPtr head = gameModel->snake;

	drawSnakeElement(head, SNAKE_HEAD);

	while (head->next != NULL)
	{
		head = head->next;
		drawSnakeElement(head, SNAKE_ELEMENT);
	}
}

void clearSnake(GameModelPtr gameModel)
{
	SnakeElementPtr head = gameModel->snake;

	drawSnakeElement(head, SNAKE_CLEAR_ELEMENT);

	while (head->next != NULL)
	{
		head = head->next;
		drawSnakeElement(head, SNAKE_CLEAR_ELEMENT);
	}
}

void extendSnakeIfFoodFound(GameModelPtr gameModel)
{	
	COORD headNextPosition = positionAfterUpdate(gameModel->snake->position, gameModel->snake->direction);

	for (int i = 0; i < MAX_FOOD_ELEMENTS_ON_SCREEN; i++)
	{
		if (headNextPosition.X == gameModel->food[i].X &&
			headNextPosition.Y == gameModel->food[i].Y) {
			gameModel->snake = extendSnake(gameModel->snake, gameModel->food[i]);
			gameModel->score++;
			break;
		}
	}
}

void moveSnake(GameModelPtr gameModel)
{
	COORD prevPosition, actPosition;

	SnakeElementPtr head = gameModel->snake;
	SnakeDirection previousDirection = head->direction;
	head->direction = gameModel->actualDirection;

	actPosition = head->position;

	updateSnakeHeadPosition(head, previousDirection);

	while (head->next != NULL)
	{
		prevPosition = actPosition;
		head = head->next;
		actPosition = head->position;
		head->position = prevPosition;
	}
}