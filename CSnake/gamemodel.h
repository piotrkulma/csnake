#pragma once

#include "snakelib.h"

typedef struct _game_model
{
	int game;
	int pause;

	int score;

	COORD* food;

	SnakeDirection actualDirection;
	SnakeElementPtr snake;
} GameModel;

typedef GameModel* GameModelPtr;

GameModelPtr initGameModel();

void updateGameState(GameModelPtr gameModel, int key);

void drawSnakeFood(GameModelPtr gameModel);
void drawSnake(GameModelPtr gameModel);
void clearSnake(GameModelPtr gameModel);

void extendSnakeIfFoodFound(GameModelPtr gameModel);
void moveSnake(GameModelPtr gameModel);
