#pragma once

#include <stdio.h>
#include <conio.h>
#include <Windows.h>

#include "config.h"

#define TRUE 1
#define FALSE 0

#define KEY_ARROW_UP 72
#define KEY_ARROW_RIGHT 77
#define KEY_ARROW_DOWN 80
#define KEY_ARROW_LEFT 75
#define KEY_ESC 27
#define KEY_ENTER 13

typedef enum _snake_direction
{
	UP, RIGHT, LEFT, DOWN
} SnakeDirection;

typedef struct _snake_element
{
	int direction;
	COORD position;
	struct _snake_element* prev;
	struct _snake_element* next;

} SnakeElement;

typedef SnakeElement* SnakeElementPtr;

void gotoxy(int x, int y);

SnakeElementPtr newSnakeElementRef();

void drawSnakeElement(SnakeElementPtr snake, char charElement);

COORD positionAfterUpdate(COORD position, SnakeDirection direction);
void updateSnakeHeadPosition(SnakeElementPtr snake, SnakeDirection previousDirection);
void updateSnakeElementPosition(SnakeElementPtr element, SnakeElementPtr prevSnakeElement);
SnakeElementPtr extendSnake(SnakeElementPtr head, COORD food);