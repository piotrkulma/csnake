#include "gamemodel.h"

void drawWalls();
void drawScore(GameModelPtr model);

void drawWalls()
{
	gotoxy(2, 1);
	printf("SCORE: ");
	
	for (int i = DISPLAY_START_WIDTH-1; i <= DISPLAY_WIDTH+1; i++)
	{
		gotoxy(i, DISPLAY_START_HEIGHT - 1);
		putch(WALL_ELEMENT);

		gotoxy(i, DISPLAY_HEIGHT + 1);
		putch(WALL_ELEMENT);

	}

	for (int i = DISPLAY_START_WIDTH-1; i <= DISPLAY_HEIGHT+1; i++)
	{
		gotoxy(DISPLAY_START_WIDTH-1, i);
		putch(WALL_ELEMENT);

		gotoxy(DISPLAY_WIDTH + 1, i);
		putch(WALL_ELEMENT);
	}
}

void drawScore(GameModelPtr model)
{
	gotoxy(8, 1);
	printf("%i", model->score);
}

int main()
{
	int key;
	int timer = 0;

	GameModelPtr gameModel = initGameModel();

	drawWalls();

	while (gameModel->game)
	{
		if (kbhit())
		{
			key = getch();
			updateGameState(gameModel, key);
		}

		if (gameModel->pause == FALSE && !kbhit())
		{
			if (timer >= 10)
			{
				clearSnake(gameModel);
				extendSnakeIfFoodFound(gameModel);
				moveSnake(gameModel);
				drawSnake(gameModel);
				drawSnakeFood(gameModel);
				drawScore(gameModel);
				timer = 0;
			}

			timer++;
			Sleep(10);
		}
	}

    return 0;
}

