#include "snakelib.h"

void gotoxy(int x, int y)
{
	COORD pos = { x, y };
	HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(output, pos);
}

SnakeElementPtr newSnakeElementRef()
{
	return  malloc(sizeof(SnakeElement) * 1);
}

SnakeElementPtr initSnake() {
	SnakeElementPtr snakeHead = newSnakeElementRef();
	COORD initPosition = { HEAD_INIT_POSITION_X, HEAD_INIT_POSITION_X };
	snakeHead->position = initPosition;
	snakeHead->direction = RIGHT;
	snakeHead->next = NULL;

	return snakeHead;
}

COORD positionAfterUpdate(COORD position, SnakeDirection direction)
{
	if (direction == UP)
	{
		position.Y--;
	}
	else if (direction == RIGHT)
	{
		position.X++;
	}
	else if (direction == DOWN)
	{
		position.Y++;
	}
	else if (direction == LEFT)
	{
		position.X--;
	}

	return position;
}

void updateSnakeHeadPosition(SnakeElementPtr snake, SnakeDirection previousDirection)
{
	 COORD posAfterUpdate = positionAfterUpdate(snake->position, previousDirection, snake->direction);

	 if (posAfterUpdate.Y < DISPLAY_START_HEIGHT)
	 {
		 posAfterUpdate.Y = DISPLAY_HEIGHT;
	 }
	 else if (posAfterUpdate.Y > DISPLAY_HEIGHT)
	 {
		 posAfterUpdate.Y = DISPLAY_START_HEIGHT;
	 }

	 if (posAfterUpdate.X< DISPLAY_START_WIDTH)
	 {
		 posAfterUpdate.X = DISPLAY_WIDTH;
	 }
	 else if (posAfterUpdate.X > DISPLAY_WIDTH)
	 {
		 posAfterUpdate.X = DISPLAY_START_WIDTH;
	 }
	 snake->position = posAfterUpdate;
}

void updateSnakeElementPosition(SnakeElementPtr element, SnakeElementPtr prevSnakeElement)
{
	element->position = prevSnakeElement->position;
}

void drawSnakeElement(SnakeElementPtr element, char charElement)
{
	gotoxy(element->position.X, element->position.Y);
	putch(charElement);
}

SnakeElementPtr extendSnake(SnakeElementPtr head, COORD food)
{
	SnakeElementPtr newElement = newSnakeElementRef();
	newElement->position = food;
	newElement->next = head;
	head = newElement;

	return head;
}